# Sistema de ventas - Servisoft

Un sistema de ventas que permite el registro y manejo de ventas de
vinos y piscos.

## Ramas

* Microservicios

    - ms-product

    - ms-person

* FrontEnd

    - servisoft-nodejs

    - servisoft-react